let sql = require('../utils/db');

let userMapper = {
    /** 로그인 */
    isExistUser: function(userEmail, userPassowrd) {
        return sql.promiseQuery("SELECT if(COUNT(*) > 0, 'true', 'false') FROM users WHERE user_email = ? AND user_password = ?", [userEmail, userPassowrd]);
    },

    /** 회원가입 */
    insertUser: function(id, hashPassword, age, phone) {
        sql.promiseQuery("INSERT INTO users(id, password, age, phone) VALUES(?, ?, ?, ?)", [id, hashPassword, age, phone]);
    },

    /** 아이디 중복체크 */
    isExistUserID: function(userEmail) {
        return sql.promiseQuery("SELECT if(COUNT(*) > 0, 'true', 'false') FROM users WHERE user_email = ?", [userEmail]);
    },

    /** 회원 정보 단건조회 */
    selectUserInfo: function(userEmail) {
        return sql.promiseQuery("SELECT user_type AS userType, user_phone AS userPhone, user_name AS userName, user_email AS userEmail FROM users WHERE user_email = ?", [userEmail]);
    }
}

module.exports = userMapper;
