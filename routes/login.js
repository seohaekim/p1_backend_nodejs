let express = require('express');
let router = express.Router();
let release = require("../utils/release");
let userMapper = require("../models/userMapper")
const crypto = require('crypto');

module.exports = router;

/** 로그인 */
router.post('', async function(req, res){
    try {
        let userEmail = req.body.userEmail; // 아이디
        let userPassword = req.body.userPassword;
        // 암호화
        // let hashPassword = crypto.createHash('sha512').update(password).digest('hex'); // 비밀번호

        if (! await userMapper.isExistUser(userEmail, userPassword)) {
            release.clientError(res, '로그인 정보가 잘못되었습니다.');
        } else {
            let result = await userMapper.selectUserInfo(userEmail);
            console.log(result)
            release.send(res, result);
        }

    } catch (e) {
        console.log("error: >>>>>>>>>> " + e)
        release.serverError(res);
    }
});

/** 로그아웃 */
router.delete('', async function(req, res){
    try {
        req.session.destroy();
        res.clearCookie('sid');
    } catch (e) {
        console.log("error: >>>>>>>>>> " + e);
        release.serverError(res);
    }
});
