let express = require('express');
let router = express.Router();
let release = require("../utils/release");
let userMapper = require("../models/userMapper")
const crypto = require('crypto');

module.exports = router;

/** 회원가입 */
router.post('', async function(req, res){
    try {
        // models
        let id = req.body.id;
        let password = req.body.password;
        let age = req.body.age;
        let phone = req.body.phone;

        // 암호화
        let hashPassword = crypto.createHash('sha512').update(password).digest('hex'); // 비밀번호

        // ID 중복확인
        if (userMapper.isExistUserID(id)) {
            release.clientError(res); // '중복된 아이디입니다.'
        } else {
            userMapper.insertUser(id, hashPassword, age, phone);
        }
    } catch (e) {
        console.log("error: >>>>>>>>>> " + e);
        release.serverError(res);
    }
});
