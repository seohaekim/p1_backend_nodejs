let express = require('express');
let router = express.Router();
let boardMapper = require("../models/boardMapper")
let release = require("../utils/release")

/** 글 등록 */
router.post('', async function(req, res){
    try {
        // models
        let title = req.body.title;
        let content = req.body.content;

        boardMapper.insertBoard(title, content, 1);

        release.send(res)
    } catch (e) {
        console.log("error: >>>>>>>>>> " + e)
        release.serverError(res)
    }
});

/** 리스트 조회 */
router.get('', async function(req, res){
    try {
        let results = await boardMapper.selectBoardList();

        release.send(res, results)
    } catch (e) {
        console.log(e + '>>>>> error')
        release.serverError(res)
    }
});

/** 글 단건조회 */
router.get('/:boardIdx', async function(req, res){
    try {
        // models
        let idx = req.params.boardIdx;

        let result = await boardMapper.selectBoard(idx)

        release.send(res, result)
    } catch (e) {
        console.log(e + '>>>>> error')
        release.serverError(res)
    }
});

/** 글 수정 */
router.put('/:idx', async function(req, res){
    try {
        // models
        let idx = req.params.idx;
        let title = req.body.title;
        let content = req.body.content;

        await boardMapper.updateBoard(title, content, idx);
        let results = await boardMapper.selectBoardList();

        release.send(res, results)
    } catch (e) {
        console.log(e + '>>>>> error')
        release.serverError(res)
    }
});

/** 글 삭제 */
router.delete('/:idx', async function(req, res){
    try {
        // models
        let idx = req.params.idx;

        await boardMapper.deleteBoard(idx);
        let results = await boardMapper.selectBoardList();

        release.send(res, results)
    } catch (e) {
        console.log(e + '>>>>> error')
        release.serverError(res)
    }
});

module.exports = router;
