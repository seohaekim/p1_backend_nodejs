let release = {
    send: function(res, result) {
        return res.status(200).json({
            result : result,
            message: 'Success'
        })
    },

    serverError: function (res) {
        return res.status(500).json({
            message: 'Server Error'
        })
    },

    clientError: function (res, result) {
        return res.status(400).json({
            result : result,
            message: 'Bad Request Error'
        })
    }
}

module.exports = release;
